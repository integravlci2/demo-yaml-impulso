# -*- coding: utf-8 -*-

# Author: Ignacio Campos
# Wellness TechGroup @ 2019

import json
import requests
import copy
import sys
from configparser import ConfigParser

if len (sys.argv) != 5 :
    print("Usage: ./device_provisioning.py config.conf devices.txt $user $password")
    sys.exit(1)

config_file = sys.argv[1]
devices_file = sys.argv[2]
user = sys.argv[3]
password = sys.argv[4]

config = ConfigParser(delimiters=('='))
config.read([config_file])

#------------------------------------------------------------------------------
### Get auth token from Keystone
#------------------------------------------------------------------------------

url_auth = config.get('keystone', 'url_auth')

payload_auth = {
    "auth": {
        "identity": {
            "methods": [
                "password"
            ],
            "password": {
                "user": {
                    "domain": {
                        "name": ""
                    },
                    "name": "",
                    "password": ""
                }
            }
        },
        "scope": {
            "project": {
                "domain": {
                    "name": ""
                },
                "name": ""
            }
        }
    }
}

payload_auth["auth"]["identity"]["password"]["user"]["domain"]["name"] = config.get('general', 'service')
payload_auth["auth"]["identity"]["password"]["user"]["name"] = user
payload_auth["auth"]["identity"]["password"]["user"]["password"] = password
payload_auth["auth"]["scope"]["project"]["domain"]["name"] = config.get('general', 'service')
payload_auth["auth"]["scope"]["project"]["name"] = config.get('general', 'subservice')

headers_auth = {
    'Content-Type': "application/json",
}

response = requests.request("POST", url_auth, data=json.dumps(payload_auth),
                            headers=headers_auth,verify=False)

token = response.headers['X-Subject-Token']
print(token)
#------------------------------------------------------------------------------
### Device provisioning into IoTAgent
#------------------------------------------------------------------------------

url_iota = config.get('iota', 'url')

headers = dict()
headers["Fiware-Service"] = config.get('general', 'service')
headers["Fiware-ServicePath"] = config.get('general', 'subservice')
headers["X-Auth-Token"] = token
headers["Content-Type"] = "application/json"

payload = {
    "devices":
    [
        {
            "device_id": "device:wt:DEVICEUID",
            "entity_name": "device:wt:DEVICEUID",
            "entity_type": "Device",
            "category": ["sensor"],
            "protocol": "IoTA-JSON",
            "transport": "MQTT",
            "commands":
            [
                {"name": "apikey","type": "command"},
                {"name": "measuremoded","type": "command"},
                {"name": "measuremodes","type": "command"},
                {"name": "measuremodec","type": "command"},
                {"name": "publishinterval","type": "command"},
                {"name": "alerts","type": "command"},
                {"name": "alertsext","type": "command"},
                {"name": "mqtt","type": "command"},
                {"name": "ntp","type": "command"},
                {"name": "batterycap","type": "command"},
                {"name": "operation","type": "command"},
                {"name": "wcontainerh","type": "command"},
                {"name": "updatesrv","type": "command"},
                {"name": "gpsen","type": "command"}
            ],
            "attributes":
            [   
                {
                    "object_id": "smcc",
                    "name": "mcc",
                    "type": "Text"
                },
                {
                    "object_id": "smnc",
                    "name": "mnc",
                    "type": "Text"
                },
                {
                    "object_id": "battery",
                    "name": "batteryLevel",
                    "type": "Number",
                    "expression": "${@batteryLevel/100}"
                },
                {
                    "object_id": "alert",
                    "name": "alert",
                    "type": "Text"
                },
                {
                    "object_id": "timestamp",
                    "name": "timestamp",
                    "type": "Text"
                },
                {
                    "object_id": "fwver",
                    "name": "fwver",
                    "type": "Text"
                },
                {
                    "object_id": "hwid",
                    "name": "hwid",
                    "type": "Text"
                },
                {
                    "object_id": "gsm",
                    "name": "gsm",
                    "type": "Text"
                },
                {
                    "object_id": "cid",
                    "name": "cid",
                    "type": "Text"
                },
                {
                    "object_id": "lac",
                    "name": "lac",
                    "type": "Text"
                },
                {
                    "object_id": "accx",
                    "name": "accx",
                    "type": "Text"
                },
                {
                    "object_id": "accy",
                    "name": "accy",
                    "type": "Text"
                },
                {
                    "object_id": "accz",
                    "name": "accz",
                    "type": "Text"
                },
                {
                    "object_id": "distance",
                    "name": "distance",
                    "type": "Text"
                },
                {
                    "object_id": "temperature",
                    "name": "temperature",
                    "type": "Number",
                    "expression": "${@temp}",
                    "entity_name": "wastecontainer:",
                    "entity_type": "WasteContainer"
                },
                {
                    "object_id": "fillingLevel",
                    "name": "fillingLevel",
                    "type": "Number",
                    "expression": "${@fill/100}",
                    "entity_name": "wastecontainer:",
                    "entity_type": "WasteContainer"
                },
                {
                    "object_id": "latitude",
                    "name": "latitude",
                    "type": "Number"
                },
                {
                    "object_id": "longitude",
                    "name": "longitude",
                    "type": "Number"
                },
                {
                    "object_id": "location",
                    "name": "location",
                    "type": "Point",
                    "expression": "[${@latitude/10000}, ${@longitude/10000}]"
                }
            ],
           "static_attributes": [
                   {"name":"controlledProperty", "value": ["temperature", "motion", "fillingLevel", "location"], "type":"ItemList"},
                   {"name":"controlledAsset", "value": ["wastecontainer:"], "type":"ItemList"},
                   {"name":"owner", "value": ["City of Valencia"], "type":"ItemList"},
                   {"name":"description", "value": "Sensor that monitorize waste containers filling level", "type":"Text"},
                   {"name":"modelName", "value": "Quamtra GPRS", "type":"Text"},
                   {"name":"brandName", "value":"Wellness TechGroup", "type":"Text"},
                   {"name":"manufacturerName", "value": "Wellness TechGroup", "type":"Text"},
                {"name":"project", "value": "Red.es", "type":"Text"},
                {"name": "dateInstalled", "type": "DateTime","value": "2019-01-01T00:00:00.00Z"}
                
           ]
        }
    ]
}

device_base = "device:wt:"

with open(devices_file, "r") as fdevices:
    devices = []
    
    for device in fdevices.readlines():
        devices.append(device_base + device.strip())

for dev in devices:
    new_payload = copy.deepcopy(payload)
    new_payload["devices"][0]["device_id"] = dev
    new_payload["devices"][0]["entity_name"] = dev
    print("Create device " + dev + " ...")
    response = requests.request("POST", url_iota, data=json.dumps(new_payload), headers=headers, verify=False)
    print(response.status_code)
